FROM openjdk:8
EXPOSE 9001
ADD target/mpaiement-0.0.1-SNAPSHOT.jar mpaiement.jar
ENTRYPOINT ["java","-jar","/mpaiement.jar"]
